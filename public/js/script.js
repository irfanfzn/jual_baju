$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
});

function del(url, dest) {
	var message = confirm('Anda yakin akan menghapus data ini?');

	if(message) {
		$.ajax({
			url: url,
			type: 'DELETE'
		});

		window.location.replace(dest);
	}
}

function del_product(elm) {	
	$(elm).parent().remove();
	var html = '<a href="#" class="add"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a>';

	$('.panjang').after(html);
}

function getPrice(elm) {
	var obj = $(elm);
	var next_obj = $(elm).next();

	$.ajax({
		dataType: "json",
		url: '/penjualan/harga',
		data: {'id' : obj.val()},
		type: 'POST',
		success: function(data) {
			// console.log(data);
			next_obj.val(data)
		}
	});
}

$(document).ready(function(e) {
	$('.datepicker').datepicker();
	$(".bahan-1").select2({ placeholder: "Pilih Bahan" });

	$('.select2').each(function(){
        $(this).select2({
            placeholder: $(this).attr('title')
        });
    });

	$(".jenis-laporan").select2();
	$('.select-report-date').hide();

	$(document).on('click', '.add', function() {
		var obj = $(this);
		var elm = $(this)[0];
		var html = '';
		var this_counter = obj.prev().prev().data('count');
		var counter = obj.prev().prev().data('count') + 1;

		html += '<div class="form-group spec-elm" data-count="'+ counter +'">';
	    html += '<label for="spec" class="col-sm-4 control-label spec-text"></label>';
	    html += '<div class="col-sm-6 select-'+ counter +'">';
	    html += '<select name="nama_bahan[]" class="bahan-'+ counter +'" data-count="'+ counter +'" style="width:123px !important; margin-right: 3px !important; display:inline-block !important">'
	    html += '<option>Pilih Bahan</option>';

	    $.each(materials, function(index, value) {
	    	html += '<option value="'+ value.val +'">'+ value.name +'</option>';
	    });

	    html += '</select>'
			html += '<input type="text" class="form-control" placeholder="Panjang" name="panjang[]" required="required" style="width:123px !important; margin-right: 3px !important; display:inline-block !important; width:100px !important">';
			html += '<a href="#" class="add"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a>';
			html += '<a href="#" class="del" onclick="del_product(this)"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></a>';
			html += '</div>';
	    html += '</div>';

	    obj.parent().parent().after(html).fadeIn('slow');
	    obj.parent().children().next().next('.add, .del').hide();


	    $('.bahan-'+ counter).select2();
	})

	$(document).on('click', '.add-jual', function() {
		var obj = $(this);
		var elm = $(this)[0];
		var html = '';
		var this_counter = obj.prev().prev().prev().prev().data('count');
		var counter = obj.prev().prev().prev().prev().data('count') + 1;

		html += '<div class="form-group spec-elm" data-count="'+ counter +'">';
	    html += '<label for="spec" class="col-sm-4 control-label spec-text"></label>';
	    html += '<div class="col-sm-6 select-'+ counter +'">';
	    html += '<select name="nama_produk[]" class="bahan-'+ counter +'" data-count="'+ counter +'" style="width:123px !important; margin-right: 3px !important; display:inline-block !important" onchange="getPrice(this)">';
	    html += '<option>Pilih Produk</option>';

	    $.each(products, function(index, value) {
	    	html += '<option value="'+ value.val +'">'+ value.name +'</option>';
	    });

	    html += '</select>'
	    html += '<input type="text" class="form-control" placeholder="harga" name="harga[]" required="required" style="width:123px !important; margin-right: 3px !important; display:inline-block !important; width:100px !important" readonly>';
	    html += '<input type="text" class="form-control" placeholder="potongan" name="potongan[]" required="required" style="width:123px !important; margin-right: 3px !important; display:inline-block !important; width:100px !important">';
	    html += '<input type="text" class="form-control" placeholder="qty" name="quantity[]" required="required" style="width:123px !important; margin-right: 3px !important; display:inline-block !important; width:50px !important">'
		html += '<a href="#" class="add-jual"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a>';
		html += '<a href="#" class="del-jual" onclick="del_product(this)"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></a>';
		html += '</div>';
    	html += '</div>';	

	    obj.parent().parent().after(html).fadeIn('slow');
	    obj.parent().children().next().next('.add-jual, .add-jual').hide();


    	$('.bahan-'+ counter).select2();
	});

	// $('.calc').click(function() {

	// });

	$('.sale-list').click(function() {
		$.ajax({
			dataType: "json",
			url: '/penjualan/detail_data',
			data: {'sale_id' : $(this).data('sale')},
			type: 'POST',
			success: function(data) {
				var html = '';
				$('.data-detail').html('');
				$.each(data.sale_details, function(index, value) {
					html += '<tr>';
					html += '<th scope="row"></th>';
					html += '<td>'+ value.name +'</td>';
					html += '<td>'+ value.price +'</td>';
					html += '<td>'+ value.sale_price +'</td>';
					html += '<td>'+ value.quantity +'</td>';
					html += '<td>'+ value.total +'</td>';
					html += '</td>';
					html += '</tr>';
				});


	        $('.data-detail').append(html);
	        $('.discount').html('<b>'+ data.sale.discount +'</b>');
	        $('.grand-total').html('<b>'+ data.sale.grand_total +'</b>');
					// next_obj.val(data)
			}
		});
	});

	$('.pm-list').click(function() {
		$.ajax({
			dataType: "json",
			url: '/produk_bahan/detail_data',
			data: {'pm_id' : $(this).data('pm')},
			type: 'POST',
			success: function(data) {
				var html = '';
				$('.data-detail').html('');
				$.each(data, function(index, value) {
					html += '<tr>';
					html += '<th scope="row"></th>';
					html += '<td>'+ value.name +'</td>';
					html += '<td>'+ value.price +'</td>';
					html += '<td>'+ value.quantity +'</td>';
					html += '<td>'+ value.remark +'</td>';
					html += '</td>';
					html += '</tr>';
				});


	        $('.data-detail').append(html);

			}
		});
	});

  $('.jenis-laporan').change(function() {
		$('.select-report-date').show();

		if($(this).val() == "") $('.select-report-date').hide();
	})
});
