@extends('layouts.app')

@section('content')

<div class="container">
    <div class="col-md-10 col-md-offset-1" style="margin-bottom:20px">
        <ul class="nav nav-pills">
          <li role="presentation"><a href="/produk_bahan/tambah">Buat Produk Bahan</a></li>
          <li role="presentation" class="active"><a href="#">List Produk Bahan</a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">List Pembuatan Produk</div>
                {!! Session::get('message') ? ShowMessage(Session::get('message'), 1) : '' !!}
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Detail</th>
                            <th>Nama Produk</th>
                            <th>Nama Bahan</th>
                            <th>Nama Konfeksi</th>
                            <th>Panjang</th>
                            <th>Tanggal</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $row)
                        <tr>
                            <th scope="row"></th>
                            <td><a href="#" class="pm-list" data-pm="{{ $row->product_id }}" data-toggle="modal" data-target="#myModal">Lihat Detail</a></td>
                            <td>{{ $row->product_name }}</td>
                            <td>{{ $row->material_name }}</td>
                            <td>{{ $row->confection_name }}</td>
                            <td>{{ $row->pm_long }}</td>
                            <td>{{ $row->pm_date }}</td>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Detail Penjualan</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama Produk</th>
                            <th>Harga</th>
                            <th>Quantitas</th>
                            <th>Catatan</th>
                        </tr>
                    </thead>
                    <tbody class="data-detail">
                    </tbody>
                </table>
            </div>

           <!--  <div class="form-horizontal">
                <div class="form-group">
                    <label for="tanggal_nota" class="col-sm-4 control-label" style="padding-top:0 !important">Diskon</label>
                    <div class="col-sm-2 discount">
                        
                    </div>
                </div>

                <div class="form-group">
                    <label for="tanggal_nota" class="col-sm-4 control-label" style="padding-top:0 !important">Grand Total</label>
                    <div class="col-sm-2 grand-total">
                        
                    </div>
                </div>
            </div> -->

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection
