@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-md-10 col-md-offset-1" style="margin-bottom:20px">
        <ul class="nav nav-pills">
          <li role="presentation" class="active"><a href="#">Buat Produk Bahan</a></li>
          <li role="presentation"><a href="/produk_bahan">List Produk Bahan</a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Input Produk Bahan</div>

                <div class="panel-body">
                    {!! Session::get('message') ? ShowMessage(Session::get('message'), 1) : '' !!}

                    {!! Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => '/produk_bahan/tambah']) !!}
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 control-label">Konfeksi</label>
                            <div class="col-sm-4">
                                <select name="nama_konfeksi" class="select2" title="Pilih Konfeksi">
                                    <option></option>
                                    @foreach($confections as $confection)
                                        <option value="{{ $confection->id }}">{{ $confection->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 control-label">Produk</label>
                            <div class="col-sm-4">
                                <select name="nama_produk" class="select2" title="Pilih Produk">
                                    <option></option>
                                    @foreach($products as $produk)
                                        <option value="{{ $produk->id }}">{{ $produk->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group spec-elm" data-count='1'>
                            <label for="spec" class="col-sm-4 control-label spec-text">Spesifikasi</label>
                            <div class="col-sm-6">
                                <select name="nama_bahan[]" class="bahan-1 " data-count='1' style="width:123px !important; display:inline-block !important">
                                    <option></option>
                                    @foreach($materials as $bahan)
                                        <option value="{{ $bahan->id }}">{{ $bahan->name }}</option>
                                    @endforeach
                                </select>

                                <input type="number" class="form-control panjang" placeholder="Panjang" name="panjang[]" required="required" style="width:123px !important; display:inline-block !important; width:100px !important">

                                <a href="#" class="add"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a>                                
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 control-label">Catatan</label>
                            <div class="col-sm-4">
                                <textarea class="form-control" rows="3" name="catatan" required="required"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="harga" class="col-sm-4 control-label">Quantity</label>
                            <div class="col-sm-4">
                                <input type="number" class="form-control" placeholder="Quantity" name="qty" required="required">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="harga" class="col-sm-4 control-label">Harga Jahit</label>
                            <div class="col-sm-4">
                                <input type="number" class="form-control" placeholder="Harga Jahit" name="upah_jahit" required="required">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="harga" class="col-sm-4 control-label">Harga Finishing</label>
                            <div class="col-sm-4">
                                <input type="number" class="form-control" placeholder="Harga Finishing" name="upah_finishing" required="required">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-10">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                    <!-- End of Form -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')

<script type="text/javascript">
var materials = [
    @foreach($materials as $bahan)
        { val : {{ $bahan->id }}, name : "{{ $bahan->name }}" },
    @endforeach
    ];
</script>

@endsection
