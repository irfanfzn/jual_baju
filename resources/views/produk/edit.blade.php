@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Produk</div>

                <div class="panel-body">
                    {!! Session::get('message') ? ShowMessage(Session::get('message'), 1) : '' !!}

                    {!! Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => "/produk/edit/$data->id", 'method' => 'put']) !!}
                        <!-- <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">App Code</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" placeholder="App Code" name="app_code" required="required" value="{{ $data->app_code }}">
                            </div>
                        </div> -->
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 control-label">Factory Code</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" placeholder="Factory Code" name="factory_code" required="required" value="{{ $data->factory_code }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 control-label">Nama</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" placeholder="Nama" name="nama" required="required" value="{{ $data->name }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 control-label">Harga</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" placeholder="Harga" name="harga" required="required" value="{{ $data->price }}">
                            </div>
                        </div>
                        <!-- <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 control-label">Gambar</label>
                            <div class="col-sm-4">
                                <input type="file" class="form-control" name="gambar">
                            </div>
                        </div> -->
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 control-label">Catatan</label>
                            <div class="col-sm-4">
                                <textarea class="form-control" rows="3" name="catatan" required="required">
                                {{ $data->remark }}
                                </textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-10">
                                <button type="button" class="btn btn-default" onClick="window.history.back();">Back</button>
                                <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                    <!-- End of Form -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
