@extends('layouts.app')

@section('content')

<div class="container">
    <div class="col-md-10 col-md-offset-1" style="margin-bottom:20px">
        <ul class="nav nav-pills">
          <li role="presentation" class="active"><a href="#">Atur Produk</a></li>
          <li role="presentation"><a href="/produk/tambah">Buat Produk</a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Atur Produk</div>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Kode App</th>
                            <th>Kode Pabrik</th>
                            <th>Nama</th>
                            <th>Harga</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $row)
                        <tr @if($row->status != 1) class="danger" @endif>
                            <th scope="row"></th>
                            <td>{{ $row->app_code }}</td>
                            <td>{{ $row->factory_code }}</td>
                            <td>{{ $row->name }}</td>
                            <td>{{ $row->price }}</td>
                            <td>@if($row->status == 1) Enable @else Disable @endif</td>
                            <td><a href="/produk/edit/{{ $row->id }}" class="btn btn-primary btn-xs">Edit</a>

                            @if($row->status == 1) 
                            <a href="#" class="btn btn-danger btn-xs" onclick="del('/produk/del/{{ $row->id }}', '/produk')">Disable</a>  
                            @else
                            <a href="#" class="btn btn-success btn-xs" onclick="del('/produk/del/{{ $row->id }}', '/produk')">Enable</a>
                            </td>
                            @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
