@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-md-10 col-md-offset-1" style="margin-bottom:20px">
        <ul class="nav nav-pills">
          <li role="presentation" class="active"><a href="#">Atur Bahan</a></li>
          <li role="presentation"><a href="/bahan/tambah">Buat Bahan</a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Atur Bahan</div>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <!-- <th>Kode App</th> -->
                            <th>Kode Pabrik</th>
                            <th>Nama</th>
                            <th>Panjang</th>
                            <th>Harga</th>
                            <th>Harga Satuan</th>
                            <!-- <th>Satuan</th> -->
                            <!-- <th>Catatan</th> -->
                            <th>Tanggal</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $row)
                        <tr @if($row->status != 1) class="danger" @endif>
                            <th scope="row"></th>
                            <!-- <td>{{ $row->app_code }}</td> -->
                            <td>{{ $row->factory_code }}</td>
                            <td>{{ $row->name }}</td>
                            <td>{{ $row->long }}</td>
                            <td>{{ $row->price }}</td>
                            <td>{{ $row->material_price }}</td>
                            <!-- <td>{{ $row->unit }}</td> -->
                            <!-- <td>{{ $row->remark }}</td> -->
                            <td>{{ $row->nota_date }}</td>
                            <td>@if($row->status == 1) Enable @else Disable @endif</td>
                            <td><a href="/bahan/edit/{{ $row->id }}" class="btn btn-primary btn-xs">Edit</a>

                            @if($row->status == 1) 
                            <a href="#" class="btn btn-danger btn-xs" onclick="del('/bahan/del/{{ $row->id }}', '/bahan')">Disable</a>  
                            @else
                            <a href="#" class="btn btn-success btn-xs" onclick="del('/bahan/del/{{ $row->id }}', '/bahan')">Enable</a>
                            </td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
