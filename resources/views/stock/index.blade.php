@extends('layouts.app')

@section('content')

<div class="container">
    <div class="col-md-10 col-md-offset-1" style="margin-bottom:20px">
        <ul class="nav nav-pills">
          <li role="presentation" class="active"><a href="#">Atur Stok</a></li>
          <li role="presentation"><a href="/stok/tambah">Buat Stok</a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Atur Stok</div>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Product</th>
                            <th>Quantity</th>
                            <th>Tanggal Nota</th>
                            <th>Catatan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $row)
                        <tr>
                            <th scope="row"></th>
                            <td>{{ $row->product->name }}</td>
                            <td>{{ $row->quantity }}</td>
                            <td>{{ $row->input_date }}</td>
                            <td>{{ $row->remark }}</td>
                            <td><a href="/stok/edit/{{ $row->id }}" class="btn btn-success btn-xs">Edit</a>
                            <a href="#" class="btn btn-danger btn-xs" onclick="del('/stok/del/{{ $row->id }}', '/stok')">Delete</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
