@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-md-10 col-md-offset-1" style="margin-bottom:20px">
        <ul class="nav nav-pills">
          <li role="presentation"><a href="/stok">Atur Stok</a></li>
          <li role="presentation" class="active"><a href="#">Buat Stok</a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Stok</div>
                <div class="panel-body">
                    {!! Session::get('message') ? ShowMessage(Session::get('message'), 1) : '' !!}

                    {!! Session::get('error') ? ErrorMessage(Session::get('error')) : '' !!}

                    {!! Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => "/stok/edit/$stock->id", 'method' => 'put']) !!}
                       <!--  <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">App Code</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" placeholder="App Code" name="app_code" required="required">
                            </div>
                        </div> -->
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 control-label">Produk</label>
                            <div class="col-sm-4">
                                <select name="nama_produk" class="select2">
                                    @foreach($products as $product)
                                        <option value="{{ $product->id }}" {{ oldSelect($product->id) }}>{{ $product->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 control-label">Quantity</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" placeholder="Quantity" name="quantity" required="required" value="{{ $stock->quantity }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 control-label">Catatan</label>
                            <div class="col-sm-4">
                                <textarea class="form-control" rows="3" name="catatan">{{ $stock->remark }}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="tanggal_nota" class="col-sm-4 control-label">Tanggal Nota</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control datepicker" placeholder="Tanggal Nota" name="tanggal_nota" required="required" data-date-format="yyyy-mm-dd" readonly value="{{ $stock->input_date }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-10">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                    <!-- End of Form -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
