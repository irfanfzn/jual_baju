@extends('layouts.app')

@section('content')
<div class="container">
    <!-- <div class="col-md-10 col-md-offset-1" style="margin-bottom:20px">
        <ul class="nav nav-pills">
          <li role="presentation" class="active"><a href="#">Buat Penjualan</a></li>
          <li role="presentation"><a href="/penjualan">List Penjualan</a></li>
        </ul>
    </div> -->
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Input Penjualan</div>

                <div class="panel-body">
                    {!! Session::get('message') ? ShowMessage(Session::get('message'), 1) : '' !!}

                    {!! Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => '/laporan']) !!}

                        <div class="form-group spec-elm" data-count='1'>
                            <label for="spec" class="col-sm-4 control-label spec-text">Laporan untuk</label>
                            <div class="col-sm-6">
                                <select name="jenis-laporan" class="jenis-laporan " data-count='1' style="width:123px !important; display:inline-block !important">
                                    <option value="">Pilih Laporan</option>
                                    <option value="bahan">Bahan</option>
                                    <option value="produk">Produk</option>
                                    <option value="konfeksi">Konfeksi</option>
                                    <option value="penjualan">Penjualan</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group select-report-date">
                            <label for="tanggal_nota" class="col-sm-4 control-label">Tanggal</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control datepicker" placeholder="Tanggal awal" name="tanggal_awal" required="required" data-date-format="yyyy-mm-dd" readonly>
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="form-control datepicker" placeholder="Tanggal akhir" name="tanggal_akhir" required="required" data-date-format="yyyy-mm-dd" readonly>
                            </div>
                        </div>

                        <!-- <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 control-label">Grand Total</label>
                            <div class="col-sm-4">

                            </div>
                        </div> -->

                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-10">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                    <!-- End of Form -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
