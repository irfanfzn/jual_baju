@extends('layouts.app')

@section('content')

<div class="container">
    <div class="col-md-10 col-md-offset-1" style="margin-bottom:20px">
        <ul class="nav nav-pills">
          <li role="presentation"><a href="/penjualan/tambah">Buat Penjualan</a></li>
          <li role="presentation" class="active"><a href="#">List Penjualan</a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Input Penjualan</div>

                
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Detail Penjualan</h4>
            </div>
            <div class="modal-body">

            </div>

            <div class="form-horizontal">
                <div class="form-group">
                    <label for="tanggal_nota" class="col-sm-4 control-label" style="padding-top:0 !important">Diskon</label>
                    <div class="col-sm-2 discount">

                    </div>
                </div>

                <div class="form-group">
                    <label for="tanggal_nota" class="col-sm-4 control-label" style="padding-top:0 !important">Grand Total</label>
                    <div class="col-sm-2 grand-total">

                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection
