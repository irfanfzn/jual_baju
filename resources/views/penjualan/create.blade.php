@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-md-10 col-md-offset-1" style="margin-bottom:20px">
        <ul class="nav nav-pills">
          <li role="presentation" class="active"><a href="#">Buat Penjualan</a></li>
          <li role="presentation"><a href="/penjualan">List Penjualan</a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Input Penjualan</div>

                <div class="panel-body">
                    {!! Session::get('message') ? ShowMessage(Session::get('message'), 1) : '' !!}

                    {!! Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => '/penjualan/tambah']) !!}

                        <div class="form-group spec-elm" data-count='1'>
                            <label for="spec" class="col-sm-4 control-label spec-text">Spesifikasi</label>
                            <div class="col-sm-6">
                                <select name="nama_produk[]" class="bahan-1 " data-count='1' style="width:123px !important; display:inline-block !important" onchange="getPrice(this)">
                                    <option></option>
                                    @foreach($products as $product)
                                        <option value="{{ $product->id }}">{{ $product->name }}</option>
                                    @endforeach
                                </select>

                                <input type="text" class="form-control" placeholder="harga" name="harga[]" required="required" style="width:123px !important; display:inline-block !important; width:100px !important" readonly>

                                <input type="number" class="form-control" placeholder="potongan" name="potongan[]" required="required" style="width:123px !important; display:inline-block !important; width:100px !important">

                                <input type="text" class="form-control" placeholder="qty" name="quantity[]" required="required" style="width:123px !important; display:inline-block !important; width:50px !important">

                                <a href="#" class="add-jual"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a>
                                <a href="#" class="del-jual" onclick="del_product(this)"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></a>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="tanggal_nota" class="col-sm-4 control-label">Tanggal Nota</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control datepicker" placeholder="Tanggal Nota" name="tanggal_nota" required="required" data-date-format="yyyy-mm-dd" readonly>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 control-label">Discount</label>
                            <div class="col-sm-2">
                                <input type="number" class="form-control" placeholder="Discount" name="discount">
                            </div>
                            <!-- <button type="button" class="btn btn-warning calc" style="width:123px !important; display:inline-block !important; width:100px !important">Kalikulasi</button> -->
                        </div>

                        <!-- <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 control-label">Grand Total</label>
                            <div class="col-sm-4">
                                
                            </div>
                        </div> -->

                        
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-10">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                    <!-- End of Form -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')

<script type="text/javascript">
var products = [
    @foreach($products as $product)
        { val : {{ $product->id }}, name : "{{ $product->name }}" },
    @endforeach
    ];
</script>

@endsection
