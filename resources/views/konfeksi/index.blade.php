@extends('layouts.app')

@section('content')

<div class="container">
    <div class="col-md-10 col-md-offset-1" style="margin-bottom:20px">
        <ul class="nav nav-pills">
          <li role="presentation" class="active"><a href="#">Atur konfeksi</a></li>
          <li role="presentation"><a href="/konfeksi/tambah">Buat konfeksi</a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Atur konfeksi</div>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Kode konfeksi</th>
                            <th>Nama</th>
                            <th>Catatan</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $row)
                        <tr @if($row->status != 1) class="danger" @endif>
                            <th scope="row"></th>
                            <td>{{ $row->confection_code }}</td>
                            <td>{{ $row->name }}</td>
                            <td>{{ $row->remark }}</td>
                            <td>@if($row->status == 1) Enable @else Disable @endif</td>
                            <td><a href="/konfeksi/edit/{{ $row->id }}" class="btn btn-primary btn-xs">Edit</a>

                            @if($row->status == 1) 
                            <a href="#" class="btn btn-danger btn-xs" onclick="del('/konfeksi/del/{{ $row->id }}', '/konfeksi')">Disable</a>  
                            @else
                            <a href="#" class="btn btn-success btn-xs" onclick="del('/konfeksi/del/{{ $row->id }}', '/konfeksi')">Enable</a>
                            </td>
                            @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
