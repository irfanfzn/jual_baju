@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-md-10 col-md-offset-1" style="margin-bottom:20px">
        <ul class="nav nav-pills">
          <li role="presentation"><a href="/konfeksi">Atur Konfeksi</a></li>
          <li role="presentation" class="active"><a href="#">Buat Konfeksi</a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Konfeksi</div>

                <div class="panel-body">
                    {!! Session::get('message') ? ShowMessage(Session::get('message'), 1) : '' !!}

                    {!! Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => "/konfeksi/edit/$data->id", 'method' => 'put']) !!}
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 control-label">Kode konfeksi</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" placeholder="Kode" name="kode_konfeksi" required="required" value="{{ $data->confection_code }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 control-label">Nama Konfeksi</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" placeholder="Nama Konfeksi" name="nama_konfeksi" required="required" value="{{ $data->name }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 control-label">Catatan</label>
                            <div class="col-sm-4">
                                <textarea class="form-control" rows="3" name="catatan">{{ $data->remark }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-10">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                    <!-- End of Form -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
