<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductFinal extends Model
{
    protected $table = 'product_finals';

    protected $guarded = ['id'];

    public static function getDetail($id) {
    	$pm_detail = Self::select(\DB::raw('
    			products.name,
    			product_finals.quantity,
    			product_finals.price,
    			product_finals.remark
    		'))
    		->join('products', 'products.id' ,'=', 'product_finals.product_id')
    		->where('product_id', $id)
    		->orderBy('product_finals.id', 'asc')
    		->get();

    	return $pm_detail;
    }
}
