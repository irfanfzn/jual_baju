<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    protected $table = 'materials';

    protected $guarded = ['id'];

    public static function generateCode() {
    	$get_code = Self::max('id');

    	if(empty($get_code)) {
    		$number = 0;
    	} else {
    		$number = $get_code;
    	}

    	$number = $number + 1;

    	return "000$number";
    }

    public static function showReport($first_date, $second_date) {
        $show_report = Self::whereBetween('created_at', [$first_date, $second_date])
                        // whereRaw("Date(created_at) = '". $first_date ."' between Date(created_at) = '". $second_date ."'")
                        ->get()
                        ->toArray();

        return $show_report;
    }

    public static function getMaterialPrice($price, $long) {
        $material_price = $price / $long;

        return $material_price;
    }
}
