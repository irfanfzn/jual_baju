<?php

/**
* Custom debug 
* @author Irfan Fauzan
*/
if (! function_exists('debug')) {
    function debug($data)
    {
        echo'<pre>';
        print_r($data);
        echo'</pre>';
    }
}

if (! function_exists('ShowMessage')) {
	function ShowMessage($message = null, $type = null)
	{
		if($type = 1)
		{
			return '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">×</button> <strong>Success!</strong> '. $message .'</div>';
		}
		else
		{
			return '<div class="alert alert-failed"> <button type="button" class="close" data-dismiss="alert">×</button> <strong>Failed!</strong> '. $message .'</div>';
		}
	}
}

if (! function_exists('ErrorMessage')) {
	function ErrorMessage($errors)
	{
		if (count($errors) > 0)
		{
			$message = '<div class="alert alert-danger">';
			$message .=	'<strong>Whoops!</strong> There were some problems with your input.<br><br>';
			$message .= '<ul>';
			
			foreach ($errors->all() as $error)
			{
				$message .= "<li>$error</li>";
			}
			
			$message .=	'</ul>';
			$message .= '</div>';

			return $message;
		}
	}
}

if (! function_exists('uploadFile')) {
	function uploadFile($file, $path)
	{
		$destination = public_path().'/'.$path;

		$mime     = $file->getClientOriginalExtension();
		$size     = $file->getSize();
		$filename = substr($file->getClientOriginalName(), 0, -5).'-'.time().'.'.$file->getClientOriginalExtension();
		$upload   = $file->move($destination, $filename);
 
		$path = $path. '/'. $filename;
	}
}

if (! function_exists('oldSelect')) {
	function oldSelect($value)
	{
		$segment = Request::segment(3);

		$result  = ($value == $segment) ? 'selected' : '';

		return $result;
	}
}

if (! function_exists('writeCsv')) {
	function writeCsv($lists, $filename)
	{
		$date 	  = Date('Y-m-d H:i:s'); 
		$filename = "uploads/report/$filename-$date.csv";
		$file 	  = fopen($filename,"w");
		$header   = (count($lists) > 1) ? array_keys($lists[1]) : array_keys($lists[0]);

		fputcsv($file, $header);

		foreach ($lists as $row)
	  	{
	  		fputcsv($file,$row);
	  	}

		fclose($file);

		return redirect($filename);
	}
}

if (! function_exists('RupiahFormat')) {
	function RupiahFormat($param)
	{
		$rupiah = "Rp " . number_format($param,2,',','.');

		return $rupiah;
	}
}
?>