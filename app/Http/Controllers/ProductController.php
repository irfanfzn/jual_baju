<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Validator;

use App\Http\Requests;

class ProductController extends Controller
{
	public function index() {
		$fetch_product = Product::paginate(10);

		return view('produk.index')->with('data', $fetch_product);
	}

    public function create() {
    	return view('produk.create');
    }

    public function store(Request $request) {
    	$rules = [
   			//'app_code' => 'required|max:255', 
			// 'factory_code' => 'required|max:30',
			'nama'   => 'required|max:255',
			'gambar' => 'image',
			'harga'  => 'numeric'
		];

		$validator = Validator::make($request->all(), $rules);

		if($validator->passes())
		{
			$app_code = Product::generateCode();

			$product = Product::create(
					[
					'app_code' 		=> $app_code, 
					'factory_code' 	=> $request->input('factory_code'), 
					'name' 			=> $request->input('nama'),
					// 'image' 		=> !empty($request->input('gambar')) ? $request->input('gambar') : '',
					'remark' 		=> $request->input('catatan'),	
					'status' 		=> 1,
					'price' 		=> $request->input('harga')
					]
				);

			// $upload = uploadFile($request->file('gambar'), 'uploads/product');

			return redirect('/produk')->with('message', 'Produk berhasil ditambahkan');
		}
		else
		{
			return redirect('/produk')->withError($validator->errors())->withInput();
		}
    }

    public function getEdit($id) {
    	$get_product = Product::where('id','=', $id)->first();

    	return view('produk.edit')->with('data', $get_product);
    }

    public function putEdit(Request $request, $id) {
    	$rules = [
   			//'app_code' => 'required|max:255', 
			// 'factory_code' => 'required|max:30',
			'nama' => 'required|max:255',
			'gambar' => 'image'
		];

		$validator = Validator::make($request->all(), $rules);

		if($validator->passes())
		{
			$product = Product::find($id);
			$product->factory_code 	= $request->input('factory_code');
			$product->name 			= $request->input('nama');
			// $product->image 		= !empty($request->input('gambar')) ? $request->input('gambar') : '';
			$product->status 		= 1;
			$product->remark 		= $request->input('catatan');
			$product->price 		= $request->input('harga');
			$product->save();

			return redirect('/produk')->with('message', 'Produk berhasil diupdate');
		}
		else
		{
			return redirect('/produk/edit/'.$id)->withError($validator->errors())->withInput();
		}
    }

    public function delete($id) {
    	$product = Product::find($id);
    	$product->status = ($product->status == 1) ? 0 : 1;
    	$product->save();

    	return redirect('/produk')->with('message', 'Produk berhasil dihapus');
    }
}
