<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Confection;

class ConfectionController extends Controller
{
    public function index() {
    	$data['data'] = Confection::paginate(10);

    	return view('konfeksi.index', $data);
    }

    public function create() {

    	return view('konfeksi.create');
    }

    public function store(Request $request) {
    	$create_conf = Confection::create([
    			'confection_code' => $request->get('kode_konfeksi'),
    			'name'            => $request->get('nama_konfeksi'),
    			'remark'          => $request->get('catatan')
    		]);

    	if($create_conf) {
    		return redirect('/konfeksi')->with('message', 'Data konfeksi berhasil ditambahkan');
    	}
    }

    public function getEdit($id) {
        $confection = Confection::where('id', $id)->first();

        return view('konfeksi.edit')->with('data', $confection);
    }

    public function putEdit(Request $request, $id) {
        
        $confection = Confection::find($id);
        $confection->name   = $request->input('nama_konfeksi');
        $confection->remark = $request->input('catatan');
        $confection->save();

        return redirect('/konfeksi')->with('message', 'Produk berhasil diupdate');
    }

    public function delete($id) {
        $confection = Confection::find($id);
        $confection->status = ($confection->status == 1) ? 0 : 1;
        $confection->save();

        return redirect('/konfeksi')->with('message', 'Produk berhasil dihapus');
    }
}
