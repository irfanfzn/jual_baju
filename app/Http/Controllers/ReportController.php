<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Product;
use App\Material;
use App\Sale;
use App\SaleDetail;
use App\Confection;

class ReportController extends Controller
{
    public function index() {
		return view('laporan.create');
    }

    public function store(Request $request) {
    	$show_report = [];
    	$report_type = $request->input('jenis-laporan');
    	$tgl_awal 	 = $request->input('tanggal_awal');
    	$tgl_akhir 	 = $request->input('tanggal_akhir');

    	switch($report_type) {
    		case 'bahan' :
    			$show_report   = Material::showReport($tgl_awal, $tgl_akhir);
    			$file_location = $report_type;
    			break;
    		case 'produk' :
    			$show_report   = Product::showReport($tgl_awal, $tgl_akhir);
    			$file_location = $report_type;
    			break;
    		case 'konfeksi' :
    			$show_report   = Confection::showReport($tgl_awal, $tgl_akhir);
    			$file_location = $report_type;
    			break;
    		case 'penjualan' :
    			$show_report   = SaleDetail::showReport($tgl_awal, $tgl_akhir);
    			$file_location = $report_type;
    			break;
    	}
        // dd($show_report);
    	$to_csv = writeCsv($show_report, $file_location);

    	return $to_csv;
    }
}
