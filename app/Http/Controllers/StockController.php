<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Stock;
use App\Product;

class StockController extends Controller
{
    public function index() {
    	$data['data'] = Stock::paginate(10);

    	return view('stock.index', $data);
    }

    public function create() {
    	$products = Product::where('status', 1)->get();

    	$data['products'] = $products;

    	return view('stock.create', $data);
    }

    public function store(Request $request) {
    	$rules = [
			'nama_produk'	=> 'required',
			'tanggal_nota' 	=> 'required',
			'quantity'		=> 'numeric'
		];

		$validator = \Validator::make($request->all(), $rules);

		if($validator->passes()) {
			$stock = Stock::create([
				'product_id' => $request->get('nama_produk'),
				'quantity' 	 => $request->get('quantity'),
				'remark' 	 => $request->get('catatan'),
				'note_date'  => $request->get('tanggal_nota'),
				'input_date' => date('Y-m-d'),
				'updated_by' => \Auth::id()
				]);

			if($stock) {
				return redirect('/stok')->with('message', 'Data stok berhasil ditambahkan');
			}
		} else {
			return redirect('/stok/tambah')->withError($validator->errors())->withInput();
		}
    }

    public function getEdit($id) {
    	$stock = Stock::where('id','=', $id)->first();

    	$products = Product::all();

    	$data['stock'] 	  = $stock;
    	$data['products'] = $products;

    	return view('stock.edit', $data);
    }

    public function putEdit(Request $request, $id) {
    	$rules = [
			'nama_produk'  => 'required',
			'tanggal_nota' => 'required',
			'quantity'		=> 'numeric'
		];

		$validator = \Validator::make($request->all(), $rules);

		if($validator->passes())
		{
			$product = Stock::find($id);
			$product->product_id = $request->input('nama_produk');
			$product->quantity 	 = $request->input('quantity');
			$product->remark 	 = $request->input('catatan');
			$product->note_date  = $request->input('tanggal_nota');
			$product->updated_by = \Auth::id();
			$product->save();

			return redirect('/stok')->with('message', 'Stok berhasil diupdate');
		}
		else
		{
			return redirect('/stok/edit/'.$id)->withError($validator->errors())->withInput();
		}
    }

    public function delete($id) {
    	$product = Stock::destroy($id);

    	return redirect('/stock')->with('message', 'Produk berhasil dihapus');
    }
}
