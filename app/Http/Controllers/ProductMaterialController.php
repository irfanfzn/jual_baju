<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Material;
use App\Product;
use App\Confection;
use App\ProductMaterial;
use App\ProductFinal;
use Validator;

class ProductMaterialController extends Controller
{
    public function index() {
        $getData = ProductMaterial::getProductMaterial();

        return view('produk-bahan.index')->with('data', $getData);
    }

    public function create() {
    	$data = [];
    	$materials = Material::all();
    	$products = Product::all();
        $confection = Confection::all();

    	$data = [
    		'materials'   => $materials,
    		'products'    => $products,
            'confections' => $confection
    	];
    	
    	return view('produk-bahan.create', $data);
    }

    public function store(Request $request) {
        $nama_bahan      = $request->get('nama_bahan');
        $panjang         = $request->get('panjang');
        $harga_jahit     = $request->get('upah_jahit');
        $quantity        = $request->get('qty');
        $harga_finishing = $request->get('upah_finishing');
        $total_salary    = ($harga_jahit + $harga_finishing) * $quantity;

        $rules = [
            //'app_code' => 'required|max:255', 
            // 'factory_code' => 'required|max:30',
            'nama_konfeksi' => 'required',
            'nama_produk' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        try {
            \DB::beginTransaction();

            for($i=0; $i<count($nama_bahan); $i++) {
                $product_material = ProductMaterial::create([
                    'product_id'    => $request->get('nama_produk'),
                    'confection_id' => $request->get('nama_konfeksi'),
                    'material_id'   => $nama_bahan[$i],
                    'long'          => $panjang[$i],
                ]);

                $update_material = Material::find($nama_bahan[$i]);
                $update_material->long = $update_material->long - $panjang[$i];
                $update_material->save();
            }

            $produk_final = ProductFinal::create([
                    'product_id'      => $request->get('nama_produk'),
                    'quantity'        => $quantity,
                    'price'           => '',
                    'remark'          => $request->get('catatan'),
                    'sewing_price'    => $harga_jahit,
                    'finishing_price' => $harga_finishing,
                    'total_salary'    => $total_salary
                ]);

            \DB::commit();

            return redirect('/produk_bahan')->with('message', 'Pembuatan bahan berhasil ditambahkan');
        } catch(\PDOException $e) {
            \DB::rollback();

            return redirect('/produk_bahan/tambah')->withError([$e])->withInput();
        }
    }

    public function getProductMaterialDetial(Request $request) {
        $id        = $request->get('pm_id');
        $pm_detail = ProductFinal::getDetail($id);

        return $pm_detail;
    }
}
