<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Product;
use App\Sale;
use App\SaleDetail;

class SalesController extends Controller
{
	public function index() {
		$sales = Sale::paginate(10);

		$data['data'] = $sales;

		return view('penjualan.index', $data);
	}

   	public function create() {
   		$products = Product::where('status', 1)->get();

   		$data['products'] = $products;

   		return view('penjualan.create', $data);
   	}

   	public function getPrice(Request $request) {
   		$product = Product::where('id', $request->get('id'))->first();

   		return $product->price;
   	}

   	public function store(Request $request) {
   		$all_total = 0;

   		$nama_produk = $request->get('nama_produk');
   		$harga       = $request->get('harga');
   		$potongan    = $request->get('potongan');
   		$tgl_nota    = $request->get('tanggal_nota');
   		$discount    = $request->get('discount');
   		$qty         = $request->get('quantity');

   		try {
            \DB::beginTransaction();

	   		$sales = new Sale;
	   		$sales->discount  = $discount;
	   		$sales->note_date = $tgl_nota;
	   		$sales->input_by  = \Auth::id();
	   		$sales->save();

	   		for($i=0; $i<count($nama_produk); $i++) {
	   			$total = ($qty[$i] * $harga[$i]) - $potongan[$i];
               // debug($total. '<br>');
               // debug($harga[$i]. ' - '. $potongan[$i] .' * '. $qty[$i] .' = '. $total);
               // dd($total);
	   			$product_material = SaleDetail::create([
	   					  'sale_id'    => $sales->id,
	                    'product_id' => $nama_produk[$i],
	                    'price'      => $harga[$i],
	                    'sale_price' => $potongan[$i],
	                    'quantity'   => $qty[$i],
	                    'total'      => $total
	                ]);

	   			$all_total += $total;
	   		}
            $grand_total = $all_total - $discount;
            // debug($grand_total);
            // dd($all_total);

			$sales->grand_total = $grand_total;
			$sales->save();

			\DB::commit();

			return redirect('/penjualan')->with('message', 'Pembuatan bahan berhasil ditambahkan');
	   	} catch(\PDOException $e) {
            \DB::rollback();

            return redirect('/penjualan')->withError([$e])->withInput();
        }
   	}

   	public function getSaleDetail(Request $request) {
   		$sale_id = $request->get('sale_id');

   		$sale_details = SaleDetail::select(\DB::raw('products.name, sale_details.*'))
   									->join('products', 'products.id', '=', 'sale_details.product_id')
   									->where('sale_id', $sale_id)
   									->get();

   		$sale = Sale::find($sale_id);

   		$data['sale_details'] = $sale_details;
   		$data['sale']         = $sale;

   		return $data;
   	}
}
