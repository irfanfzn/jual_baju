<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Material;
use Validator;

use App\Http\Requests;

class MaterialController extends Controller
{
	public function index() {
		$fetch_product = Material::paginate(10);

		return view('bahan.index')->with('data', $fetch_product);
	}

    public function create() {
    	return view('bahan.create');
    }

    public function store(Request $request) {
    	$harga   = $request->input('harga');
    	$panjang = $request->input('panjang');

    	$rules = [
   			//'app_code' => 'required|max:255', 
			// 'factory_code' => 'required|max:30',
			'nama' 	  	   => 'required|max:255',
			'tanggal_nota' => 'required',
			'gambar'  	   => 'image',
			'harga'   	   => 'numeric',
			'panjang' 	   => 'numeric'
		];

		$validator = Validator::make($request->all(), $rules);

		if($validator->passes())
		{
			$app_code = Material::generateCode();

			$getMaterialPrice = Material::getMaterialPrice($harga, $panjang);

			$product = Material::create(
					[
					'app_code' 		 => $app_code, 
					'factory_code' 	 => $request->input('factory_code'), 
					'name' 			 => $request->input('nama'),
					'long' 			 => $request->input('panjang'),
					'unit' 			 => 'Yard',
					'remark' 		 => $request->input('catatan'),	
					'status' 		 => 1,
					'price' 		 => $request->input('harga'),
					'nota_date' 	 => $request->input('tanggal_nota'),
					'material_price' => $getMaterialPrice
					// 'image' 		=> !empty($request->input('gambar')) ? $request->input('gambar') : '',
					]
				);

			// $upload = uploadFile($request->file('gambar'), 'uploads/material');

			return redirect('/bahan/tambah')->with('message', 'Bahan berhasil ditambahkan');
		}
		else
		{
			return redirect('/bahan/tambah')->withError($validator->errors())->withInput();
		}
    }

    public function getEdit($id) {
    	$get_product = Material::where('id','=', $id)->first();

    	return view('bahan.edit')->with('data', $get_product);
    }

    public function putEdit(Request $request, $id) {
    	$harga   = $request->input('harga');
    	$panjang = $request->input('panjang');

    	$rules = [
   			//'app_code' => 'required|max:255', 
			// 'factory_code' => 'required|max:30',
			'nama' 	  	   => 'required|max:255',
			'tanggal_nota' => 'required',
			'harga'   	   => 'numeric',
			'panjang' 	   => 'numeric'
		];

		$validator = Validator::make($request->all(), $rules);

		if($validator->passes())
		{
			$getMaterialPrice = Material::getMaterialPrice($harga, $panjang);

			$product = Material::find($id);
			$product->factory_code 	 = $request->input('factory_code');
			$product->name 		   	 = $request->input('nama');
			$product->unit 		   	 = $request->input('satuan');
			$product->long 		   	 = $request->input('panjang');
			$product->remark 	   	 = $request->input('catatan');
			$product->status 	   	 = 1;
			$product->price 	   	 = $request->input('harga');
			$product->nota_date    	 = $request->input('tanggal_nota');
			$product->material_price = $getMaterialPrice;
			// $product->image 	   = !empty($request->input('gambar')) ? $request->input('gambar') : '';
			$product->save();

			return redirect('/bahan')->with('message', 'Bahan berhasil diupdate');
		}
		else
		{
			return redirect('/bahan/edit/'.$id)->withError($validator->errors())->withInput();
		}
    }

    public function delete($id) {
    	$product = Material::find($id);
    	$product->status = ($product->status == 1) ? 0 : 1;
    	$product->save();

    	return redirect('/bahan')->with('message', 'Bahan berhasil dihapus');
    }

    public function testCsv() {
    	$materials = Material::all()->toArray();
		debug($materials);
		$import = writeCsv($materials, 'material.csv');
    }
}
