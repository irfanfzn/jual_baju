<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::auth();

Route::get('/home', 'HomeController@index');

Route::get('/bahan', 'MaterialController@index');
Route::get('/bahan/edit/{id}', 'MaterialController@getEdit');
Route::put('/bahan/edit/{id}', 'MaterialController@putEdit');
Route::delete('/bahan/del/{id}', 'MaterialController@delete');
Route::get('/bahan/tambah', 'MaterialController@create');
Route::post('/bahan/tambah', 'MaterialController@store');

Route::get('/produk', 'ProductController@index');
Route::get('/produk/edit/{id}', 'ProductController@getEdit');
Route::put('/produk/edit/{id}', 'ProductController@putEdit');
Route::delete('/produk/del/{id}', 'ProductController@delete');
Route::get('/produk/tambah', 'ProductController@create');
Route::post('/produk/tambah', 'ProductController@store');

Route::get('/produk_bahan/tambah', 'ProductMaterialController@create');
Route::post('/produk_bahan/tambah', 'ProductMaterialController@store');
Route::get('/produk_bahan', 'ProductMaterialController@index');
Route::post('/produk_bahan/detail_data', 'ProductMaterialController@getProductMaterialDetial');

Route::get('/konfeksi', 'ConfectionController@index');
Route::get('/konfeksi/tambah', 'ConfectionController@create');
Route::post('/konfeksi/tambah', 'ConfectionController@store');
Route::get('/konfeksi/edit/{id}', 'ConfectionController@getEdit');
Route::put('/konfeksi/edit/{id}', 'ConfectionController@putEdit');
Route::delete('/konfeksi/del/{id}', 'ConfectionController@delete');

Route::get('/stok', 'StockController@index');
Route::get('/stok/tambah', 'StockController@create');
Route::post('/stok/tambah', 'StockController@store');
Route::get('/stok/edit/{id}', 'StockController@getEdit');
Route::put('/stok/edit/{id}', 'StockController@putEdit');
Route::delete('/stok/del/{id}', 'StockController@delete');

Route::get('/penjualan', 'SalesController@index');
Route::get('/penjualan/tambah', 'SalesController@create');
Route::post('/penjualan/tambah', 'SalesController@store');
Route::post('/penjualan/harga', 'SalesController@getPrice');
Route::post('/penjualan/detail_data', 'SalesController@getSaleDetail');

Route::get('/laporan', 'ReportController@index');
Route::post('/laporan', 'ReportController@store');

Route::get('/test_csv', 'MaterialController@testCsv');
