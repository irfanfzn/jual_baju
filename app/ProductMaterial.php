<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductMaterial extends Model
{
    protected $table = 'product_materials';

    protected $guarded = ['id'];

    public static function getProductMaterial() {
    	$prod_mater = Self::select(\DB::raw('
    		products.name as product_name,
    		product_materials.product_id as product_id,
    		materials.name as material_name,
    		confections.name as confection_name,
    		product_materials.long as pm_long,
    		product_materials.created_at pm_date
    		'))
    		->join('products', 'products.id', '=', 'product_materials.product_id')
    		->join('materials', 'materials.id', '=', 'product_materials.material_id')
    		->join('confections', 'confections.id', '=', 'product_materials.confection_id')
    		->orderBy('product_materials.id', 'asc')
    		->get();

    	return $prod_mater;
    } 
}
