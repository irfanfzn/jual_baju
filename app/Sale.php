<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $table = 'sales';

    protected $guarded = ['id'];

    public function sale_detail() {
    	return $this->hasMany('App\SaleDetail');
    }
}
