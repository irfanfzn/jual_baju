<?php

namespace App;

use App\SaleDetail;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    public static function drawCsv($data) {
		fputcsv($out, array_keys($data[1]));

		$out = fopen('php://output', 'w');

		foreach($data as $line)
		{
			fputcsv($out, $line);
		}

		fclose($out);
    }
}
