<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $guarded = ['id'];

    public function stock() {
        return $this->hasMany('App\Stock');
    }

    public function sale_detail() {
        return $this->hasMany('App\SaleDetail');
    }

    public static function generateCode() {
    	$get_code = Self::max('id');

    	if(empty($get_code)) {
    		$number = 0;
    	} else {
    		$number = $get_code;
    	}

    	$number = $number + 1;

    	return "000$number";
    }

    public static function showReport($first_date, $second_date) {
        $show_report = Self::whereBetween('created_at', [$first_date, $second_date])
                        ->get()
                        ->toArray();

        return $show_report;
    }
}
