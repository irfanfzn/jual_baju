<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaleDetail extends Model
{
    protected $table = 'sale_details';

    protected $guarded = ['id'];

    public function sale() {
    	return $this->belongsTo('App\Sale');
    }

    public function product() {
    	return $this->belongsTo('App\Product');
    }

    public static function showReport($first_date, $second_date) {
        $show_report = Self::select(\DB::raw('products.`name`, 
							SUM(`sale_details`.`price`) as total_price,
							SUM(`sale_details`.`sale_price`) as total_sale_price,
							`sale_details`.`quantity` as qty,
							SUM(`sale_details`.`total`) as total,
							sales.`discount`,
							sales.`grand_total`,
							sales.created_at'))
        				->join('sales', 'sales.id', '=', 'sale_details.sale_id')
        				->join('products', 'products.id', '=', 'sale_details.product_id')
        				->whereBetween('sales.created_at', [$first_date, $second_date])
        				->groupBy('products.name')
                        ->get()
                        ->toArray();

        return $show_report;
    }
}
