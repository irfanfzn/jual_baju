<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Confection extends Model
{
    protected $table = 'confections';

    protected $guarded = ['id'];

    public static function showReport($first_date, $second_date) {
        $show_report = Self::whereBetween('created_at', [$first_date, $second_date])
                        ->get()
                        ->toArray();

        return $show_report;
    }
}
